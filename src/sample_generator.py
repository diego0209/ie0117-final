import numpy as np
import matplotlib.pyplot as plt

def generateNormalDistributionSample(mean, stddev, size, file_path):
    """Generates sample of random values with normal distribution.

    Args:
        mean: mean of the sample.
        stddev: standard deviation of the sample.
        size: sample size.
        file_path: file to write.

    Returns:
        None.
    """

    sample = np.random.normal(mean, stddev, size)
    file = open(file_path, 'w')
    for value in sample:
        file.write("%f" % value)
        file.write("\n")
    file.close()
    print("File written in ", file_path)
    return None

def generateBinomialDistributionSample(n, p, size, file_path):
    """Generates sample of random values with binomial distribution.

    Args:
        n: mean of the sample.
        p: standard deviation of the sample.
        size: sample size.
        file_path: file to write.

    Returns:
        None.
    """

    sample = np.random.binomial(n, p, size)
    file = open(file_path, 'w')
    for value in sample:
        file.write("%f" % value)
        file.write("\n")
    file.close()
    print("File written in ", file_path)
    return None

def generateChiSquareDistributionSample(df, size, file_path):
    """Generates sample of random values with normal distribution.

    Args:
        df: degrees of freedom.
        size: sample size.
        file_path: file to write.

    Returns:
        None.
    """

    sample = np.random.chisquare(df, size)
    file = open(file_path, 'w')
    for value in sample:
        file.write("%f" % value)
        file.write("\n")
    file.close()
    print("File written in ", file_path)
    return None

def generateLaplaceDistributionSample(mean, scale, size, file_path):
    """Generates sample of random values with normal distribution.

    Args:
        mean: mean of the sample.
        scale: the exponential decay.
        size: sample size.
        file_path: file to write.

    Returns:
        None.
    """

    sample = np.random.laplace(mean, scale, size)
    file = open(file_path, 'w')
    for value in sample:
        file.write("%f" % value)
        file.write("\n")
    file.close()
    print("File written in ", file_path)
    return None

def generateHistogramFromFile(file_path, bins):
    """Generates a histogram from a sample read from a file.

    Args:
        file_path: file to read.
        bins: number of bins for histogram.

    Returns:
        None.
    """

    file = open(file_path,'r')
    sample = []
    for line in file:
        sample.append(float(line))
    file.close()
    plt.hist(sample, bins=bins, orientation='vertical')
    plt.title(file_path)
    plt.show()
    del sample
    return None

def main():
    generateNormalDistributionSample(0, 1, 10, "m1.txt")
    generateNormalDistributionSample(0, 1, 100, "m2.txt")
    generateNormalDistributionSample(0, 1, 1000, "m3.txt")
    generateNormalDistributionSample(5, 3, 100, "m4.txt")
    generateNormalDistributionSample(5, 3, 1000, "m5.txt")
    generateHistogramFromFile("m1.txt", 10)
    generateHistogramFromFile("m2.txt", 30)
    generateHistogramFromFile("m3.txt", 100)
    generateHistogramFromFile("m4.txt", 20)
    generateHistogramFromFile("m5.txt", 100)
    generateBinomialDistributionSample(50, 0.5, 15, "o1.txt")
    generateBinomialDistributionSample(50, 0.5, 500, "o2.txt")
    generateChiSquareDistributionSample(5, 15, "o3.txt")
    generateChiSquareDistributionSample(5, 500, "o4.txt")
    generateLaplaceDistributionSample(0, 1, 15, "o5.txt")
    generateLaplaceDistributionSample(0, 1, 500, "o6.txt")
    generateHistogramFromFile("o1.txt", 11)
    generateHistogramFromFile("o2.txt", 20)
    generateHistogramFromFile("o3.txt", 10)
    generateHistogramFromFile("o4.txt", 35)
    generateHistogramFromFile("o5.txt", 9)
    generateHistogramFromFile("o6.txt", 50)

if __name__ == '__main__':
    main()
