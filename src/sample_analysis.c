#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/**
 * Stores the data read from a file to dynamic memory.
 * @param filePath	file path of the file to read.
 * @param listPtr	pointer to the pointer that points to the first element of the array.
 * @returns number of values read.
 * @retval 0 if memory was allocated successfully.
 */
int storeIntoHeap(char const* filePath, double** listPtr){
	int counter = 0;
	FILE* file;
	file = fopen(filePath, "r");
	if(file) {
		for (char c = getc(file); c != EOF; c = getc(file)){
			if (c == '\n'){
				counter+=1;
			}
		}
		*listPtr = malloc(sizeof(double)*counter);
		if(listPtr){
			double value;
			int index = 0;
			rewind(file);
			int check = fscanf(file, "%lf", &value);
			while( check != EOF ){
				*(*listPtr+index) = value;
				index+=1;
				check = fscanf(file, "%lf", &value);
			}
		} else {
			printf("Error: not enough memory.\n");
		}
		fclose(file);
	} else {
		printf("No such file.\n");
	}
	return counter;
}

/**
 * Calculates the mean for a data set.
 * @param listPtr			pointer to the first element of the array.
 * @param numberOfValues	amount of values in the array.
 * @returns the mean of the data set.
 */
double getMean(double* listPtr, int numberOfValues){
	double sum = 0;
	for (int index = 0; index < numberOfValues; index++){
		sum = sum + listPtr[index];
	}
	return sum/(double)numberOfValues;
}

/**
 * Calculates the standard deviation for a data set.
 * @param listPtr			pointer to the first element of the array.
 * @param numberOfValues	amount of values in the array.
 * @param mean				mean of the data set.
 * @returns the standard deviation of the data set.
 */
double getStandardDeviation(double* listPtr, int numberOfValues, double mean){
	double sum = 0;
	for (int index = 0; index < numberOfValues; index++){
		sum = sum + pow((listPtr[index]-mean), 2);
	}
	return sqrt((sum/(double)numberOfValues));
}

/**
 * Calculates the value for m_k for a data set.
 * @param k					m's subscript.
 * @param listPtr			pointer to the first element of the array.
 * @param numberOfValues	amount of values in the array.
 * @param mean				mean of the data set.
 * @returns m_k value.
 */
double getM_(int k, double* listPtr, int numberOfValues, double mean){
	double sum = 0;
	for (int index = 0; index < numberOfValues; index++){
		sum = sum + pow((listPtr[index]-mean), k);
	}
	return (sum/(double)numberOfValues);
}

/**
 * Calculates the third moment for a data set.
 * @param listPtr			pointer to the first element of the array.
 * @param numberOfValues	amount of values in the array.
 * @param mean				mean of the data set.
 * @returns the third moment of the data set.
 */
double getThirdMoment(double* listPtr, int numberOfValues, double mean){
	return ( getM_(3, listPtr, numberOfValues, mean) / pow( (getM_(2, listPtr, numberOfValues, mean)) , (3/2)) );
}

/**
 * Calculates the fourth moment for a data set.
 * @param listPtr			pointer to the first element of the array.
 * @param numberOfValues	amount of values in the array.
 * @param mean				mean of the data set.
 * @returns the fourth moment of the data set.
 */
double getFourthMoment(double* listPtr, int numberOfValues, double mean){
	return ( getM_(4, listPtr, numberOfValues, mean) / pow( (getM_(2, listPtr, numberOfValues, mean)) , 2) );
}

int main(int argc, char const *argv[]) {
	argc--, argv++;	// skips over program name.
	double* list = NULL;
	if(argc>=1){
		int numberOfValues = storeIntoHeap(argv[0], &list);
		if(list) {
			printf("Data set: %s\n", argv[0]);
			printf("Amount of values: %i\n", numberOfValues);
			double mean = getMean(list, numberOfValues);
			printf("Mean: %lf\n", mean);
			printf("Standard Deviation: %lf\n", getStandardDeviation(list, numberOfValues, mean));
			printf("Third moment: %lf\n", getThirdMoment(list, numberOfValues, mean));
			printf("Fourth moment: %lf\n", getFourthMoment(list, numberOfValues, mean));
			free(list);
		}
	}
	return 0;
}
