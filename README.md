# Programación Bajo Plataformas Abiertas IE-0117 - Final exam
Final exam of the course Programación Bajo Plataformas Abiertas IE-0117. Universidad de Costa Rica I-2018.

Analyzer of data sets generated with different probability distributions.

## Required packages:
* python3
* python3-numpy
* python3-matplotlib
* gcc
* make